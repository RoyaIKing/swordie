package enums;

/**
 * Created on 3/25/2018.
 */
public enum FieldInstanceType {
    CHANNEL,
    EXPEDITION,
    PARTY,
    SOLO
}
