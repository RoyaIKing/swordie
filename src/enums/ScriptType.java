package enums;

/**
 * Created on 2/19/2018.
 */
public enum ScriptType {
    NONE,
    NPC,
    FIELD,
    PORTAL,
    REACTOR,
    ITEM,
    QUEST
}
